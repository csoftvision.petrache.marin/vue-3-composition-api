import { watchEffect, ref } from 'vue';

export default function useSearch(data, searchTerm) {
  const searchQuery = ref('');
  const filteredList = ref(null);

  watchEffect(() => {
    if (data.value && data.value.length) {
      filteredList.value = data.value.filter(item => {
        return item[searchTerm].toLowerCase().includes(searchQuery.value.toLowerCase())
      });
    }
  });

  // watch(data, (newValue) => {
  //   if (newValue && newValue.length) {
  //     filteredList.value = newValue.filter(item => {
  //       return item[searchTerm].toLowerCase().includes(searchQuery.value.toLowerCase())
  //     })
  //   }
  // });
  
  return {
    searchQuery,
    filteredList
  };
}
