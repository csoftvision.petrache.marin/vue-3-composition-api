import { ref } from 'vue';

export default function useFetch(url) {
  const results = ref(null);
  const isLoading = ref(true);
  const error = ref(null);
  

  fetch(url)
    .then(response => response.json())
    .then(data => {
      // special check for imdb api
      if (data.Search) {
        results.value = data.Search;
      } else {
        results.value = data;
      }
    })
    .catch(err => {
      error.value = err;
    })
    .finally(() => {
      isLoading.value = false;
    });

  return {
    results,
    isLoading,
    error
  };
}