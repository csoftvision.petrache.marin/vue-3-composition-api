import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "Movies",
    component: () => import("../views/Movies.vue"),
  },
  {
    path: "/movie/:id",
    name: "Movie",
    component: () => import("../views/Movie.vue"),
  },
  {
    path: "/posts",
    name: "Posts",
    component: () => import("../views/Posts.vue"),
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
